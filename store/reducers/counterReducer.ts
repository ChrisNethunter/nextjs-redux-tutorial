import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from '../store'

export interface CounterState {
  value: number
}

const initialState: CounterState = {
  value: 0,
}

export const counterReducer = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment: (state) => {
      state.value += 1
    },
    decrement: (state) => {
      state.value -= 1
    }
  },
})

export const { increment, decrement  } = counterReducer.actions
export const selectValue = (state: RootState) => state.counter.value;

export default counterReducer.reducer